let canvas = document.getElementById('line-canvas')
let ctx = canvas.getContext('2d')
let randX, randY, intervalID, newX, newY
let i = 0
let running = true
const rMax = 150,
  rMin = -150

let currOrigin = {
  x: canvas.width / 2,
  y: canvas.height / 2,
}

function drawLines() {
  randX =
    Math.floor(Math.random() * (Math.floor(rMax) - Math.ceil(rMin) + 1)) + rMin
  randY =
    Math.floor(Math.random() * (Math.floor(rMax) - Math.ceil(rMin) + 1)) + rMin

  ctx.strokeStyle =
    'rgb(0, ' + Math.floor(255 - 12.75 * i) + ', ' + Math.floor(12.75 * i) + ')'
  ctx.beginPath()
  ctx.moveTo(currOrigin['x'], currOrigin['y'])

  // boundary checks
  if (currOrigin['x'] + randX > canvas.width || currOrigin['x'] + randX < 0) {
    newX = currOrigin['x'] - randX
  } else {
    newX = currOrigin['x'] + randX
  }
  if (currOrigin['y'] + randY > canvas.height || currOrigin['y'] + randY < 0) {
    newY = currOrigin['y'] - randY
  } else {
    newY = currOrigin['y'] + randY
  }

  ctx.lineTo(newX, newY)
  ctx.stroke()

  currOrigin['x'] = newX
  currOrigin['y'] = newY

  // set rgb multiplier
  i > 20 ? (i = 0) : (i += 1)
}

// assumes running is starting on true
let toggleRun = function (e) {
  if (!running || !intervalID) {
    intervalID = window.setInterval(drawLines, 1000)
    running = true
  } else {
    window.clearInterval( intervalID )
    running = false
  }
}

canvas.addEventListener('click', toggleRun)
intervalID = window.setInterval(drawLines, 1000)
